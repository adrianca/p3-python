from mysound import Sound


# hay que comparar las duraciones y quedarnos con la mayor,
# sumando hasta la duración del menor
def soundadd(sound1: Sound, sound2: Sound) -> Sound:
    if sound1.duration < sound2.duration:
        sound = Sound(sound2.duration)
        for i in range(sound2.nsamples):
            if i < sound1.nsamples:
                sound.buffer[i] = sound1.buffer[i] + sound2.buffer[i]
            else:
                sound.buffer.append(sound2.buffer[i])
    else:
        sound = Sound(sound1.duration)
        for i in range(sound1.nsamples):
            if i < sound2.nsamples:
                sound.buffer[i] = sound1.buffer[i] + sound2.buffer[i]
            else:
                sound.buffer.append(sound1.buffer[i])

    return sound


# aqui pruebo con un ejemplo para ver si se ha hecho correctamente,
# y abro el buffer para comprobarlo
sound1 = Sound(1)
sound1.sin(440, 10)
sound2 = Sound(1)
sound2.sin(100, 5)
finalSound = soundadd(sound1, sound2)
print(finalSound.buffer)
