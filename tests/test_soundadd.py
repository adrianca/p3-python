import unittest
from mysound import Sound
from soundops import soundadd

class TestSoundAdd(unittest.TestCase):
    #Sumo dos sonidos de igual duración y compruebo que la suma tiene la misma duración que el primero de ellos
    def test_add_same_length(self):
        sound1 = Sound(1)
        sound2 = Sound(1)
        result = soundadd(sound1, sound2)
        self.assertEqual(len(result.buffer), len(sound1.buffer))
    #Sumo dos sonidos de diferentes duraciones y compruebo que la suma tiene una duración diferente a los otros dos
    def test_add_different_lengths(self):
        sound1 = Sound(0.5)
        sound2 = Sound(1)
        result = soundadd(sound1, sound2)
        self.assertNotEquals(len(result.buffer), (len(sound2.buffer) or len(sound1.buffer)))

    #Sumo dos sonidos y compruebo que la suma es una instancia de Sound
    def test_add_two_sounds(self):
        sound1 = Sound(1)
        sound2 = Sound(1)

        result = soundadd(sound1, sound2)

        self.assertIsInstance(result, Sound)

if __name__ == '__main__':
    unittest.main()
