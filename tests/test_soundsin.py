import unittest
from mysoundsin import SoundSin

class TestSoundSin(unittest.TestCase):
    # voy a verificar que la duracion del sonido sea correcta
    def test_duration(self):
        duration = 3
        frequency = 440
        amplitude = 1
        sound_sin = SoundSin(duration, frequency, amplitude)
        self.assertEqual(sound_sin.duration, duration)

    class TestSoundSin(unittest.TestCase):
        # voy a verificar que la amplitud va variando entre valores positivos y negativos
        def test_amplitude_sign(self):
            sound = SoundSin(2, 440, 1)
            self.assertLessEqual(min(sound.buffer), 0)
            self.assertGreaterEqual(max(sound.buffer), 0)

    # Ahora verifico que se crean las barras siendo una instancia de Sound, y no es una cadena vacia
    def test_bars_output(self):
        duration = 2
        frequency = 440
        amplitude = 1
        sound_sin = SoundSin(duration, frequency, amplitude)
        bars_output = sound_sin.bars(bar_period=0.01)
        self.assertIsInstance(bars_output, str)
        self.assertGreater(len(bars_output), 0)


if __name__ == '__main__':
    unittest.main()
