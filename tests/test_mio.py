import unittest
from mysound import Sound


class testMio(unittest.TestCase):

    #Vamos a comprobar que el tamaño del buffer es el mismo que el número de muestras por segundo (sample rate),
    #multiplicado por la duración

    def test_samples_rate(self):
        sound = Sound(7)

        self.assertEqual(len(sound.buffer), 7 * Sound.samples_second)


if __name__ == '__main__':
    unittest.main()
